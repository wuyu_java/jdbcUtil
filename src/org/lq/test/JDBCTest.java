package org.lq.test;

import static org.junit.Assert.assertEquals;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.junit.Test;
import org.lq.util.JDBCUtil;
import org.lq.util.ResultSetCreateObject;

public class JDBCTest {
	@Test
	public void add() {
		String sql = "insert into dept(dname,description)values(?,?)";
		try {
			int num = JDBCUtil.executeUpdate(sql, "名字","描述");
			assertEquals(num, 1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	@Test
	public void delete() {
		try {
			int num =JDBCUtil.executeUpdate("delete from dept where dno = ?", 6);
			assertEquals(num, 1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void update() {
		try {
			int num =JDBCUtil.executeUpdate("update dept set dname =?,description=? where dno = ?", "吃饭部-修改","专业厨师",4);
			assertEquals(num, 1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void findAll() {
		try {
			
			List<Dept> list = JDBCUtil.executeQuery("select * from dept", new ResultSetCreateObject<Dept>() {
				@Override
				public Dept create(ResultSet rs)throws SQLException {
					Dept d = new Dept();
					d.setDescription(rs.getString("description"));
					d.setDname(rs.getString("dname"));
					d.setDno(rs.getInt("dno"));
					return d;
				}
			});
			for (Dept dept : list) {
				System.out.println(dept);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void find() {
		
		try {
			List<Person> list = JDBCUtil.executeQuery("select * from person", new ResultSetCreateObject<Person>() {

				@Override
				public Person create(ResultSet rs) throws SQLException {
					Person p = new Person();
					p.setAddress(rs.getString("address"));
					p.setEducation(rs.getString("education"));
					p.setGender(rs.getString("gender"));
					p.setId(rs.getInt("id"));
					p.setName(rs.getString("name"));
					return p;
				}
			});
			
			for (Person person : list) {
				System.out.println(person);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	@Test
	public void find2() {
		
		try {
			List<Person> list = JDBCUtil.executeQuery("select * from person where name like ?", new ResultSetCreateObject<Person>() {

				@Override
				public Person create(ResultSet rs) throws SQLException {
					Person p = new Person();
					p.setAddress(rs.getString("address"));
					p.setEducation(rs.getString("education"));
					p.setGender(rs.getString("gender"));
					p.setId(rs.getInt("id"));
					p.setName(rs.getString("name"));
					return p;
				}
			},"%无%");
			
			for (Person person : list) {
				System.out.println(person);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	

}
