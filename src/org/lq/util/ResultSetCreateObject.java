package org.lq.util;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface ResultSetCreateObject<T> {
	
	/**
	 * 根据ResultSet进行对象创建
	 * @param <T>
	 * @param rs
	 * @return
	 */
	T create(ResultSet rs) throws SQLException;

}
